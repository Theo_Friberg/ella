# Ella #

Päätin nimetä vempaimen Ella-äänen mukaan. Setuppi vaatii numpyn. Sen saa asentamalla pipin:

(OSX)


```
#!bash

# download and install setuptools
curl -O https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py
python3 ez_setup.py
# download and install pip
curl -O https://bootstrap.pypa.io/get-pip.py
python3 get-pip.py
```

(Ubunttu)


```
#!bash

sudo apt-get install python3-pip
```

Ja sitten pipillä itse numpy:

```
#!bash

sudo pip3 install numpy
```

Sitten repon voi git cloneta.

Testaamisenhaluiset voivat ajaa repon sisällä:

```
python -i friberg.py
f = fribergText()
f.learn(["Prompti"], ["Vastaus"], 1)

# [ctrl] + [c] halutessa

f.runThrough("Haluttu syöte")

```