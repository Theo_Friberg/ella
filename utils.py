import numpy as np

def toInt(char):
    if char == ' ':
        return 26
    elif char == "'":
        return 27
    elif char == '.':
        return 28
    elif char == ',':
        return 29
    elif char == '_':
        return 30
    elif char.isalpha():
        return ord(char.lower()) - 97
    else:
        return 31

def toChar(integer):
    if integer == 26:
        return ' '
    elif integer == 27:
        return "'"
    elif integer == 28:
        return '.'
    elif integer == 29:
        return ','
    elif integer == 30:
        return '_'
    elif integer == 31:
        return '?'
    else:
        return chr(integer + 97)

def toNumpyArrays(string):
    arrays = []
    for x in range(len(string)):
        arrays.append(np.zeros(32))
    for i in range(len(string)):
        arrays[i][toInt(string[i])] = 1.0
    return arrays

def toString(numpyArr):
    letters = ""
    for letter in numpyArr:
        letters = letters + toChar(np.where(letter==np.max(letter))[0][0])
    return letters
