import numpy as np

import random

import utils

class fribergModel():

    def __init__(self, widths):
        self.multipliersCurrent = []
        self.multipliersPrev = []
        self.widths = widths
        prev = widths[0]
        self.biases = []
        for layer in widths:
            self.multipliersCurrent.append(np.random.rand(layer*prev)-0.5)
            self.multipliersPrev.append(np.random.rand(layer*prev)-0.5)
            #self.multipliersCurrent.append(np.array([0.5]*layer*prev))
            #self.multipliersPrev.append(np.array([0.5]*layer*prev))
            self.biases.append(np.zeros(layer))
            prev = layer
        self.prev = np.array([0.5]*widths[0])

    def clear(self):
        prev = self.widths[0]

    def evaluate(self, data):

        currentData = data
        prevData = self.prev
        for current, previous, biases in zip(self.multipliersCurrent, self.multipliersPrev, self.biases):
            prevData = np.tanh(np.sum(np.reshape(previous, (len(previous)//len(prevData), len(prevData)))*prevData, axis = 1)+biases)
            currentData = np.tanh(np.sum(np.reshape(current, (len(current)//len(currentData), len(currentData)))*currentData, axis = 1)+biases) * 0.5 + prevData * 0.5

        result = currentData
        self.prev = self.prev*0.8 + data*0.2# * np.sum(result) / self.widths[-1]

        return result

class fribergText():

    def __init__(self):

        self.rnn = fribergModel([32, 32, 32, 32])

    def _getArr(self, string):
        self.rnn.clear()
        result = []
        arr = utils.toNumpyArrays(string)

        # Priming loop (let the nn get an idea of the input)

        for letter in arr:
            self.rnn.evaluate(letter)

        # Output loop (let the nn treat letters)

        for i in range(50):
            letter = np.zeros(32)
            if i < len(arr):
                letter = arr[i]
            result.append(self.rnn.evaluate(letter))

        return result

    def runThrough(self, string):
        return utils.toString(self._getArr(string))

    def errorFunction(self, string, right):
        output = self._getArr(string)
        if len(right) > 50:
            right = right[:50]
        elif len(right) < 50:
            output = output[:len(right)]

        result = np.sum(np.abs(np.array(output) - np.array(right)), axis=1).flatten()
        return np.sum(result)/len(result)

    def collectiveError(self, inputs, outputs):
        results = []
        for inputData, outputData in zip(inputs, outputs):
            results.append(self.errorFunction(inputData, utils.toNumpyArrays(outputData)))
        return results[0]

    def learn(self, inputs, outputs, generations, rate, biasRate=0.02):
        for generation in range(generations):
            for layer in range(len(self.rnn.multipliersCurrent))[::-1]:
                prevErr = self.collectiveError(inputs, outputs)
                for multiplier in range(len(self.rnn.multipliersCurrent[layer])):
                    change = rate*random.random()
                    self.rnn.multipliersCurrent[layer][multiplier] = self.rnn.multipliersCurrent[layer][multiplier] - change
                    err1 = self.collectiveError(inputs, outputs)
                    self.rnn.multipliersCurrent[layer][multiplier] = self.rnn.multipliersCurrent[layer][multiplier] + 2 * change
                    err2 = self.collectiveError(inputs, outputs)
                    if min(err1, err2) < prevErr:
                        if err1 < err2:
                            self.rnn.multipliersCurrent[layer][multiplier] = self.rnn.multipliersCurrent[layer][multiplier] - 2 * change
                        else:
                            pass
                    else:
                        self.rnn.multipliersCurrent[layer][multiplier] = self.rnn.multipliersCurrent[layer][multiplier] - change

                    change2 = random.random()*rate
                    self.rnn.multipliersPrev[layer][multiplier] = self.rnn.multipliersPrev[layer][multiplier] - change2
                    err3 = self.collectiveError(inputs, outputs)
                    self.rnn.multipliersPrev[layer][multiplier] = self.rnn.multipliersPrev[layer][multiplier] + 2 * change2
                    err4 = self.collectiveError(inputs, outputs)
                    if min(err3, err4, err1, err2, prevErr) != min(err1, err2, prevErr):
                        if err3 < err4:
                            self.rnn.multipliersPrev[layer][multiplier] = self.rnn.multipliersPrev[layer][multiplier] - 2 * change2
                        else:
                            pass
                    else:
                        self.rnn.multipliersPrev[layer][multiplier] = self.rnn.multipliersPrev[layer][multiplier] - change2

                    if True:#multiplier % self.rnn.widths[layer] == 0:
                        self.rnn.biases[layer][multiplier // self.rnn.widths[layer]] = self.rnn.biases[layer][multiplier // self.rnn.widths[layer]] + biasRate
                        err6 = self.collectiveError(inputs, outputs)
                        self.rnn.biases[layer][multiplier // self.rnn.widths[layer]] = self.rnn.biases[layer][multiplier // self.rnn.widths[layer]] - 2*biasRate
                        err7 = self.collectiveError(inputs, outputs)
                        if min(err3, err4, err1, err2, err6, err7, prevErr) == min(err6, err7):
                            if err6 < err7:
                                self.rnn.biases[layer][multiplier // self.rnn.widths[layer]] = self.rnn.biases[layer][multiplier // self.rnn.widths[layer]] + 2*biasRate
                            else:
                                pass
                        else:
                            self.rnn.biases[layer][multiplier // self.rnn.widths[layer]] = self.rnn.biases[layer][multiplier // self.rnn.widths[layer]] + biasRate

                    print(self.runThrough(inputs[0]))
                    #print(str(err1) + " " + str(err2))
                    prevErr = min(err1, err2, err3, err4, prevErr)
                    print(str(prevErr) + " " + str(layer + 1) + " / " + str(len(self.rnn.multipliersCurrent)) + " " + str(multiplier) + " / " + str(str(len(self.rnn.multipliersCurrent[layer]))) + " " + str(generation + 1) + " / " + str(generations))
                
